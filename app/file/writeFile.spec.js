const fs = require('fs');
const request = require('request');
const expect = require('chai').expect;
const writeFile = require('./writeFile');

describe('The webpage module', function () {
  it('saves the content', function* () {
    const url = 'google.com',
      content = '<h1>title</h1>',
      writeFileStub = this.sandbox.stub(fs, 'writeFile').callsFake(function (filePath, fileContent, cb) {
        cb(null);
      }),
      requestStub = this.sandbox.stub(request, 'get').callsFake(function (url, cb) {
        cb(null, null, content);
      }),
      result = yield writeFile.saveWebpage(url);

    expect(writeFileStub).to.be.calledWith();
    expect(requestStub).to.be.calledWith(url);
    expect(result).to.eql('page');
  });
});