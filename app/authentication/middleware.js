const authenticationMiddleware = () => {
  const middleware = (req, res, next) => {
    if(req.isAuthenticated())
      return next();

    res.redirect('/');
  }

  return middleware;
}